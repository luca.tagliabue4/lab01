package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.IntPredicate;

public class GitRepository {

	private String repoPath;

	public GitRepository(String nRepoPath) {
		repoPath = nRepoPath;
	}

	public String getHeadRef() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(repoPath + "/HEAD"));
		return br.readLine().split(" ")[1];
	}

	public String getRefHash(String string) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(repoPath + "/" + string));
		return br.readLine();
	}

}
